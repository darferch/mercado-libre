package com.dariochamorro.data.datasources

import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.models.ProductDetail

interface ProductRemoteDataSource {

    suspend fun listProductsByQuery(query: String): List<Product>
    suspend fun getProductDetail(id: String): ProductDetail
}
