package com.dariochamorro.data

import com.dariochamorro.data.datasources.ProductLocalDataSource
import com.dariochamorro.data.datasources.ProductRemoteDataSource
import com.dariochamorro.data.datasources.SearchLocalDataSource
import com.dariochamorro.data.repositories.ProductRepositoryImpl
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.AsyncResultStatus
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.models.ProductDetail
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class ProductRepositoryImplTest : CoroutineUnitTest() {

    @Mock
    lateinit var searchLocalDataSource: SearchLocalDataSource

    @Mock
    lateinit var productRemoteDataSource: ProductRemoteDataSource

    @Mock
    lateinit var productLocalDataSource: ProductLocalDataSource

    lateinit var productRepositoryImpl: ProductRepositoryImpl

    @Before
    fun setup() {
        productRepositoryImpl = ProductRepositoryImpl(
            productLocalDataSource, productRemoteDataSource, searchLocalDataSource
        )
    }

    @Test
    fun `productsByQuery should return a list of products by query`() = runBlockingTest {
        // Given
        val testQuery = "test query"
        val data = SampleFactory.list(Product::class)
        whenever(productRemoteDataSource.listProductsByQuery(testQuery)).thenReturn(data)

        // When
        val result = productRepositoryImpl.productsByQuery(testQuery)

        // Then
        assertEquals(AsyncResultStatus.SUCCESS, result.status)
        assertEquals(data, result.data)
    }

    @Test
    fun `productsByQuery should save the query locally`() = runBlockingTest {
        // Given
        val testQuery = "test query"

        // When
        productRepositoryImpl.productsByQuery(testQuery)

        // Then
        verify(searchLocalDataSource, atLeastOnce()).addSearchToHistory(testQuery)
    }

    @Test
    fun `productsByQuery should return an error if listProductsByQuery fails`() = runBlockingTest {
        // Given
        val testQuery = "test query"
        whenever(productRemoteDataSource.listProductsByQuery(testQuery)).thenThrow(
            IllegalStateException()
        )

        // When
        val result = productRepositoryImpl.productsByQuery(testQuery)

        // Then
        assertEquals(AsyncResultStatus.ERROR, result.status)
    }

    @Test
    fun `productsByQuery should return an error if addSearchToHistory fails`() = runBlockingTest {
        val testQuery = "test query"
        whenever(searchLocalDataSource.addSearchToHistory(testQuery)).thenThrow(
            IllegalStateException()
        )

        // When
        val result = productRepositoryImpl.productsByQuery(testQuery)

        // Then
        assertEquals(AsyncResultStatus.ERROR, result.status)
    }

    @Test
    fun `productDetailById should return a product detail by id`() = runBlockingTest {
        // Given
        val testId = "testId"
        val data = SampleFactory.single(ProductDetail::class)
        whenever(productRemoteDataSource.getProductDetail(testId)).thenReturn(data)

        // When
        val result = productRepositoryImpl.productDetailById(testId)

        // Then
        assertEquals(AsyncResultStatus.SUCCESS, result.status)
        assertEquals(data, result.data)
    }

    @Test
    fun `productDetailById should return an error if getProductDetail fails`() = runBlockingTest {
        val testId = "testId"
        whenever(productRemoteDataSource.getProductDetail(testId)).thenThrow(
            IllegalStateException()
        )

        // When
        val result = productRepositoryImpl.productDetailById(testId)

        // Then
        assertEquals(AsyncResultStatus.ERROR, result.status)
    }

    @Test
    fun `recentProducts should emit a list of products`() = runBlockingTest {
        // Given
        val data = SampleFactory.list(Product::class)
        val expectedResult = AsyncResult.success(data)
        whenever(productLocalDataSource.listProducts()).thenReturn(listOf(data).asFlow())

        // When
        var result: AsyncResult<List<Product>>? = null
        productRepositoryImpl.recentProducts().collect { result = it }

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `recentProducts should emit an error if listProducts fails`() = runBlockingTest {
        // Given
        val expectedResult = AsyncResult.error<List<Product>>(Errors.DatabaseError)
        whenever(productLocalDataSource.listProducts()).thenReturn(flow { throw IllegalStateException() })

        // When
        var result: AsyncResult<List<Product>>? = null
        productRepositoryImpl.recentProducts().collect { result = it }

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `deleteProductFromRecent should delete a product locally`() = runBlockingTest {
        // Given
        val data = SampleFactory.single(Product::class)

        // When
        val result = productRepositoryImpl.deleteProductFromRecent(data)

        // Then
        assertEquals(AsyncResultStatus.SUCCESS, result.status)
        verify(productLocalDataSource, atLeastOnce()).deleteProduct(data.id)
    }

    @Test
    fun `deleteProductFromRecent should emit an error if deleteProduct fails`() = runBlockingTest {
        val data = SampleFactory.single(Product::class)
        whenever(productLocalDataSource.deleteProduct(data.id)).thenThrow(
            IllegalStateException()
        )

        // When
        val result = productRepositoryImpl.deleteProductFromRecent(data)

        // Then
        assertEquals(AsyncResultStatus.ERROR, result.status)
    }
}
