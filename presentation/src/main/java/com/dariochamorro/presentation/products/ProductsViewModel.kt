package com.dariochamorro.presentation.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.usescases.GetProductsByQueryUseCase
import com.dariochamorro.domain.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val getProductsByQueryUseCase: GetProductsByQueryUseCase,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    private val productsControl: MutableLiveData<AsyncResult<List<Product>>> = MutableLiveData()
    val products: LiveData<AsyncResult<List<Product>>> = productsControl

    fun queryProducts(query: String) = viewModelScope.launch(dispatchers.main()) {
        productsControl.value = AsyncResult.loading()
        productsControl.value = getProductsByQueryUseCase(query)
    }
}
