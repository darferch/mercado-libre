package com.dariochamorro.presentation.common.errors

import com.dariochamorro.domain.common.Errors

interface ErrorMessage {
    fun getMessage(error: Errors?): String
}
