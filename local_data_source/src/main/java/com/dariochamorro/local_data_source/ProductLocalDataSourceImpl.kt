package com.dariochamorro.local_data_source

import com.dariochamorro.data.datasources.ProductLocalDataSource
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.models.ProductDetail
import com.dariochamorro.local_data_source.dao.ProductDao
import com.dariochamorro.local_data_source.mappers.ProductDetailMapper
import com.dariochamorro.local_data_source.mappers.ProductEntityMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ProductLocalDataSourceImpl @Inject constructor(
    private val dao: ProductDao,
    private val productEntityMapper: ProductEntityMapper,
    private val productDetailMapper: ProductDetailMapper
) : ProductLocalDataSource {

    override fun listProducts(): Flow<List<Product>> = dao.listProductDescending()
        .map(productEntityMapper::toListModel)

    override suspend fun addProduct(productDetail: ProductDetail) {
        val product = productDetailMapper.fromModel(productDetail)
        dao.insert(product)
    }

    override suspend fun deleteProduct(productId: String) {
        dao.deleteByProductId(productId)
    }
}
