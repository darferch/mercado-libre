package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.ProductDetail
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.usescases.GetProductDetailByIdUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetProductDetailByIdUseCase : CoroutineUnitTest() {
    @Mock
    lateinit var productRepository: ProductRepository
    lateinit var getProductDetailByIdUseCase: GetProductDetailByIdUseCase

    @Before
    fun setup() {
        getProductDetailByIdUseCase =
            GetProductDetailByIdUseCase(productRepository, testDispatchers)
    }

    private val testId = "1"

    @Test
    fun `should return a ProductDetail if it is invoked`() =
        runBlockingTest {
            // Given
            val data = SampleFactory.single(ProductDetail::class)
            val resultExpected = AsyncResult.success(data)
            whenever(productRepository.productDetailById(testId)).thenReturn(resultExpected)

            // When
            val result = getProductDetailByIdUseCase(testId)

            // Then
            assertEquals(resultExpected, result)
        }

    @Test
    fun `should return a AsyncResult Error if productDetailById fail`() = runBlockingTest {
        // Given
        val resultExpected = AsyncResult.error<ProductDetail>(Errors.NetworkError)
        whenever(productRepository.productDetailById(testId)).thenReturn(resultExpected)

        // When
        val result = getProductDetailByIdUseCase(testId)

        // Then
        assertEquals(resultExpected, result)
    }
}
