package com.dariochamorro.domain.models

data class ProductDetail(
    val id: String,
    val title: String,
    val price: Double,
    val availableQuantity: Int,
    val soldQuantity: Int,
    val condition: String,
    val images: List<String>,
    val thumbnail: String,
    val description: String?,
    val attributes: List<ProductAttribute>,
    val warranty: String
)
