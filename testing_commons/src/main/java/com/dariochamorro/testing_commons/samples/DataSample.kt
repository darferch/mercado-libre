package com.dariochamorro.testing_commons.samples

abstract class DataSample<T> {

    abstract fun single(index: Int = 1): T

    fun list(size: Int = 1): List<T> = (0..size).map(this::single)
}
