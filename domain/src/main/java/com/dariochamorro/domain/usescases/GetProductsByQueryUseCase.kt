package com.dariochamorro.domain.usescases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetProductsByQueryUseCase @Inject constructor(
    private val productRepository: ProductRepository,
    private val dispatchers: DispatcherProvider
) {
    suspend operator fun invoke(query: String): AsyncResult<List<Product>> =
        withContext(dispatchers.io()) {
            productRepository.productsByQuery(query)
        }
}
