package com.dariochamorro.domain.models

data class ProductAttribute(
    val id: String,
    val name: String,
    val value: String
)
