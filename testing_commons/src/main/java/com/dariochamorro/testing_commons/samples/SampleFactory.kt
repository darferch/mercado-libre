package com.dariochamorro.testing_commons.samples

import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.models.ProductDetail
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
object SampleFactory {

    private val productSample = ProductSample()
    private val productDetailSample = ProductDetailSample()
    private val stringSample = StringSample()

    fun <T : Any> single(kClass: KClass<T>): T = when (kClass) {
        Product::class -> productSample.single() as T
        ProductDetail::class -> productDetailSample.single() as T
        String::class -> stringSample.single() as T
        else -> throw IllegalArgumentException("Class not have samples")
    }

    fun <T : Any> list(kClass: KClass<T>, size: Int = 1): List<T> = when (kClass) {
        Product::class -> productSample.list(size) as List<T>
        ProductDetail::class -> productDetailSample.list(size) as List<T>
        String::class -> stringSample.list(size) as List<T>
        else -> throw IllegalArgumentException("Class not have samples")
    }
}
