package com.dariochamorro.data.di

import com.dariochamorro.data.repositories.ProductRepositoryImpl
import com.dariochamorro.data.repositories.SearchHistoryRepositoryImpl
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.repositories.SearchHistoryRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoriesModule {

    @Binds
    @Singleton
    abstract fun bindProductRepository(productRepositoryImpl: ProductRepositoryImpl): ProductRepository

    @Binds
    @Singleton
    abstract fun bindSearchHistoryRepository(searchHistoryRepositoryImpl: SearchHistoryRepositoryImpl): SearchHistoryRepository
}
