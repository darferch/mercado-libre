package com.dariochamorro.domain.repositories

import com.dariochamorro.domain.common.AsyncResult

interface SearchHistoryRepository {

    suspend fun searchHistoryByQuery(query: String): AsyncResult<List<String>>
}
