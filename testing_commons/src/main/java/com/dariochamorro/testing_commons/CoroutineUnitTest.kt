package com.dariochamorro.testing_commons

import com.dariochamorro.testing_commons.rules.CoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule

@ExperimentalCoroutinesApi
open class CoroutineUnitTest : UnitTest() {

    @get:Rule
    val coroutineDispatcherRule = CoroutineRule()
    val testDispatchers = coroutineDispatcherRule.testDispatchers
}
