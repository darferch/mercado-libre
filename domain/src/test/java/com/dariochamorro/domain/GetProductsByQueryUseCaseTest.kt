package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.usescases.GetProductsByQueryUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetProductsByQueryUseCaseTest : CoroutineUnitTest() {
    @Mock
    lateinit var repository: ProductRepository
    lateinit var getProductsByQueryUseCase: GetProductsByQueryUseCase

    @Before
    fun setup() {
        getProductsByQueryUseCase =
            GetProductsByQueryUseCase(repository, testDispatchers)
    }

    @Test
    fun `should return a list of products when it is invoked`() =
        runBlockingTest {
            // Given
            val testQuery = "test query"
            val data = SampleFactory.list(Product::class, 2)
            val resultExpected = AsyncResult.success(data)
            whenever(repository.productsByQuery(testQuery)).thenReturn(resultExpected)

            // When
            val result = getProductsByQueryUseCase(testQuery)

            // Then
            assertEquals(resultExpected, result)
        }

    @Test
    fun `should return a AsyncResult Error if productsByQuery fail`() = runBlockingTest {
        // Given
        val testQuery = "test query"
        val resultExpected = AsyncResult.error<List<Product>>(Errors.NetworkError)
        whenever(repository.productsByQuery(testQuery)).thenReturn(resultExpected)

        // When
        val result = getProductsByQueryUseCase(testQuery)

        // Then
        assertEquals(resultExpected, result)
    }
}
