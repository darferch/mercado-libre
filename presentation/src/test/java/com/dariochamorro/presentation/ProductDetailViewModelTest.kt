package com.dariochamorro.presentation

import com.dariochamorro.android_testing_commons.LiveDataUnitTest
import com.dariochamorro.android_testing_commons.util.getOrAwaitValue
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.ProductDetail
import com.dariochamorro.domain.usescases.GetProductDetailByIdUseCase
import com.dariochamorro.presentation.product_detail.ProductDetailViewModel
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class ProductDetailViewModelTest : LiveDataUnitTest() {

    @Mock
    lateinit var getProductDetailByIdUseCase: GetProductDetailByIdUseCase

    lateinit var viewModel: ProductDetailViewModel

    private val testId = "testId"

    @Before
    fun setup() {
        viewModel =
            ProductDetailViewModel(getProductDetailByIdUseCase, testDispatchers)
    }

    @Test
    fun `loadProductDetail should emit AsyncResult with loading status through a liveData`() {
        // Given
        val expectedResult = AsyncResult.loading<ProductDetail>()

        // When
        val result: AsyncResult<ProductDetail> = viewModel.productDetail.getOrAwaitValue {
            viewModel.loadProductDetail(testId)
        }

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `loadProductDetail should emit AsyncResult with data status through a liveData`() =
        runBlockingTest {
            // Given
            val data = SampleFactory.single(ProductDetail::class)
            val expectedResult = AsyncResult.success(data)

            whenever(getProductDetailByIdUseCase(testId)).thenReturn(expectedResult)

            // When
            viewModel.loadProductDetail(testId)
            val result = viewModel.productDetail.getOrAwaitValue()

            // Then
            assertEquals(expectedResult, result)
        }
}
