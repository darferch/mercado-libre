package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.repositories.SearchHistoryRepository
import com.dariochamorro.domain.usescases.GetSearchHistoryByQueryUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetSearchHistoryByQueryUseCaseTest : CoroutineUnitTest() {
    @Mock
    lateinit var repository: SearchHistoryRepository
    lateinit var getSearchHistoryByQueryUseCase: GetSearchHistoryByQueryUseCase

    private val query = "Query Test"

    @Before
    fun setup() {
        getSearchHistoryByQueryUseCase = GetSearchHistoryByQueryUseCase(repository, testDispatchers)
    }

    @Test
    fun `should return a list of strings if is invoked`() =
        runBlockingTest {
            // Given
            val data = SampleFactory.list(String::class, 2)
            val resultExpected = AsyncResult.success(data)
            whenever(repository.searchHistoryByQuery(query)).thenReturn(resultExpected)

            // When
            val result = getSearchHistoryByQueryUseCase(query)

            // Then
            assertEquals(resultExpected, result)
        }

    @Test
    fun `should return a AsyncResult Error if searchHistoryByQuery fail`() = runBlockingTest {
        // Given
        val resultExpected = AsyncResult.error<List<String>>(Errors.NetworkError)
        whenever(repository.searchHistoryByQuery(query)).thenReturn(resultExpected)

        // When
        val result = getSearchHistoryByQueryUseCase(query)

        // Then
        assertEquals(resultExpected, result)
    }
}
