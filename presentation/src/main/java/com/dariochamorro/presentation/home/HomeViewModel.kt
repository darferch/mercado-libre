package com.dariochamorro.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.usescases.DeleteProductFromRecentUseCase
import com.dariochamorro.domain.usescases.GetRecentProductsUseCase
import com.dariochamorro.domain.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    getRecentProductsUseCase: GetRecentProductsUseCase,
    private val deleteProductFromRecentUseCase: DeleteProductFromRecentUseCase,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    val recentProducts: LiveData<AsyncResult<List<Product>>> = getRecentProductsUseCase()
        .asLiveData(timeoutInMs = 500)

    fun deleteProduct(product: Product) = viewModelScope.launch(dispatchers.main()) {
        deleteProductFromRecentUseCase(product)
    }
}
