package com.dariochamorro.data

import com.dariochamorro.data.datasources.SearchLocalDataSource
import com.dariochamorro.data.repositories.SearchHistoryRepositoryImpl
import com.dariochamorro.domain.common.AsyncResultStatus
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class SearchHistoryRepositoryImplTest : CoroutineUnitTest() {

    @Mock
    lateinit var searchLocalDataSource: SearchLocalDataSource

    lateinit var searchHistoryRepositoryImpl: SearchHistoryRepositoryImpl

    @Before
    fun setup() {
        searchHistoryRepositoryImpl = SearchHistoryRepositoryImpl(searchLocalDataSource)
    }

    @Test
    fun `searchHistoryByQuery should return a list of strings by query`() = runBlockingTest {
        // Given
        val testQuery = "test query"
        val data = SampleFactory.list(String::class, 2)
        whenever(searchLocalDataSource.searchHistoryByQuery(testQuery)).thenReturn(data)

        // When
        val result = searchHistoryRepositoryImpl.searchHistoryByQuery(testQuery)

        // Then
        Assert.assertEquals(AsyncResultStatus.SUCCESS, result.status)
        Assert.assertEquals(data, result.data)
    }

    @Test
    fun `searchHistoryByQuery return an error if searchHistoryByQuery fails`() = runBlockingTest {
        val testQuery = "test query"
        whenever(searchLocalDataSource.searchHistoryByQuery(testQuery)).thenThrow(
            IllegalStateException()
        )

        // When
        val result = searchHistoryRepositoryImpl.searchHistoryByQuery(testQuery)

        // Then
        Assert.assertEquals(AsyncResultStatus.ERROR, result.status)
    }
}
