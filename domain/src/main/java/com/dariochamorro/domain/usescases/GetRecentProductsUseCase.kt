package com.dariochamorro.domain.usescases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetRecentProductsUseCase @Inject constructor(
    private val productRepository: ProductRepository,
    private val dispatchers: DispatcherProvider
) {
    operator fun invoke(): Flow<AsyncResult<List<Product>>> = productRepository.recentProducts()
        .flowOn(dispatchers.io())
}
