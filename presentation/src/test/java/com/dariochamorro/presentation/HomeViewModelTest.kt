package com.dariochamorro.presentation

import com.dariochamorro.android_testing_commons.LiveDataUnitTest
import com.dariochamorro.android_testing_commons.util.getOrAwaitValue
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.usescases.DeleteProductFromRecentUseCase
import com.dariochamorro.domain.usescases.GetRecentProductsUseCase
import com.dariochamorro.presentation.home.HomeViewModel
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class HomeViewModelTest : LiveDataUnitTest() {

    @Mock
    lateinit var getRecentProductsUseCase: GetRecentProductsUseCase

    @Mock
    lateinit var deleteProductFromRecentUseCase: DeleteProductFromRecentUseCase

    lateinit var viewModel: HomeViewModel

    lateinit var productData: List<Product>

    @Before
    fun setup() {
        productData = SampleFactory.list(Product::class, 2)
        val result = AsyncResult.success(productData)
        whenever(getRecentProductsUseCase()).thenReturn(listOf(result).asFlow())

        viewModel =
            HomeViewModel(getRecentProductsUseCase, deleteProductFromRecentUseCase, testDispatchers)
    }

    @Test
    fun `recentProducts should emit AsyncResult with the recent products`() = runBlockingTest {
        // Given
        val expectedResult = AsyncResult.success(productData)

        // When
        val result: AsyncResult<List<Product>> = viewModel.recentProducts.getOrAwaitValue()

        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `deleteProduct should call deleteProductFromRecent Use Case`() = runBlockingTest {
        // Give
        val data = SampleFactory.single(Product::class)

        // When
        viewModel.deleteProduct(data)

        // Then
        verify(deleteProductFromRecentUseCase, atLeastOnce()).invoke(data)
    }
}
