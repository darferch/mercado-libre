package com.dariochamorro.presentation

import com.dariochamorro.android_testing_commons.LiveDataUnitTest
import com.dariochamorro.android_testing_commons.util.getOrAwaitValue
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.usescases.GetSearchHistoryByQueryUseCase
import com.dariochamorro.presentation.search.SearchViewModel
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class SearchViewModelTest : LiveDataUnitTest() {

    @Mock
    lateinit var getSearchHistoryByQueryUseCase: GetSearchHistoryByQueryUseCase
    lateinit var viewModel: SearchViewModel

    @Before
    fun setup() {
        viewModel = SearchViewModel(getSearchHistoryByQueryUseCase, testDispatchers)
    }

    @Test
    fun `loadSearchHistory should emit AsyncResult with data status through a liveData`() =
        runBlockingTest {
            // Given
            val testQuery = "test query"
            val data = SampleFactory.list(String::class)
            val expectedResult = AsyncResult.success(data)

            whenever(getSearchHistoryByQueryUseCase(testQuery)).thenReturn(expectedResult)

            // When
            viewModel.loadSearchHistory(testQuery)
            val result = viewModel.searchHistory.getOrAwaitValue()

            // Then
            Assert.assertEquals(expectedResult, result)
        }
}
