package com.dariochamorro.data.repositories

import com.dariochamorro.data.datasources.SearchLocalDataSource
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.repositories.SearchHistoryRepository
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchHistoryRepositoryImpl @Inject constructor(
    private val localDataSource: SearchLocalDataSource
) : SearchHistoryRepository {

    private val logger = Logger.getLogger(SearchHistoryRepositoryImpl::class.simpleName)

    override suspend fun searchHistoryByQuery(query: String): AsyncResult<List<String>> = try {
        val result = localDataSource.searchHistoryByQuery(query)
        AsyncResult.success(result)
    } catch (e: Exception) {
        logger.warning(e.message)
        AsyncResult.error(Errors.DatabaseError)
    }
}
