package com.dariochamorro.mercadolibre.di

import com.dariochamorro.data.di.RepositoriesModule
import com.dariochamorro.domain.di.DomainUtilModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(
    includes = [
        DomainUtilModule::class,
        RepositoriesModule::class
    ]
)
@InstallIn(SingletonComponent::class)
object AppModule
