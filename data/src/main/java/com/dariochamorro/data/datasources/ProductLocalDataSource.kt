package com.dariochamorro.data.datasources

import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.models.ProductDetail
import kotlinx.coroutines.flow.Flow

interface ProductLocalDataSource {

    fun listProducts(): Flow<List<Product>>
    suspend fun addProduct(productDetail: ProductDetail)
    suspend fun deleteProduct(productId: String)
}
