package com.dariochamorro.testing_commons.samples

import com.dariochamorro.domain.models.ProductAttribute
import com.dariochamorro.domain.models.ProductDetail

class ProductDetailSample : DataSample<ProductDetail>() {
    override fun single(index: Int): ProductDetail = ProductDetail(
        id = "$index",
        title = "Title $index",
        price = 1000.0 * (index + 1),
        availableQuantity = index + 1,
        soldQuantity = index,
        condition = "Condition $index",
        images = (0..2).map { "Image $index $it" },
        thumbnail = "thumbnail $index",
        description = "Description $index",
        attributes = (0..2).map {
            ProductAttribute(
                id = "${index}_$it",
                name = "Attribute ${index}_$it",
                value = "Value ${index}_$it"
            )
        },
        warranty = "Warranty $index"
    )
}
