package com.dariochamorro.presentation

import com.dariochamorro.android_testing_commons.LiveDataUnitTest
import com.dariochamorro.android_testing_commons.util.getOrAwaitValue
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.usescases.GetProductsByQueryUseCase
import com.dariochamorro.presentation.products.ProductsViewModel
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class ProductsViewModelTest : LiveDataUnitTest() {

    @Mock
    lateinit var getProductsByQueryUseCase: GetProductsByQueryUseCase
    lateinit var viewModel: ProductsViewModel

    private val testQuery = "test query"

    @Before
    fun setup() {
        viewModel = ProductsViewModel(getProductsByQueryUseCase, testDispatchers)
    }

    @Test
    fun `queryProducts should emit AsyncResult with loading status through a liveData`() {
        // Given
        val expectedResult = AsyncResult.loading<List<Product>>()

        // When
        val result: AsyncResult<List<Product>> = viewModel.products.getOrAwaitValue {
            viewModel.queryProducts(testQuery)
        }

        // Then
        Assert.assertEquals(expectedResult, result)
    }

    @Test
    fun `queryProducts should emit AsyncResult with data status through a liveData`() =
        runBlockingTest {
            // Given
            val data = SampleFactory.list(Product::class)
            val expectedResult = AsyncResult.success(data)

            whenever(getProductsByQueryUseCase(testQuery)).thenReturn(expectedResult)

            // When
            viewModel.queryProducts(testQuery)
            val result = viewModel.products.getOrAwaitValue()

            // Then
            Assert.assertEquals(expectedResult, result)
        }
}
