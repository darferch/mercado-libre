package com.dariochamorro.presentation.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.usescases.GetSearchHistoryByQueryUseCase
import com.dariochamorro.domain.util.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val getSearchHistoryByQueryUseCase: GetSearchHistoryByQueryUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val searchHistoryControl: MutableLiveData<AsyncResult<List<String>>> = MutableLiveData()
    val searchHistory: LiveData<AsyncResult<List<String>>> = searchHistoryControl

    fun loadSearchHistory(query: String) = viewModelScope.launch(dispatcher.main()) {
        searchHistoryControl.value = getSearchHistoryByQueryUseCase(query)
    }
}
