package com.dariochamorro.local_data_source.di

import com.dariochamorro.data.datasources.ProductLocalDataSource
import com.dariochamorro.data.datasources.SearchLocalDataSource
import com.dariochamorro.local_data_source.ProductLocalDataSourceImpl
import com.dariochamorro.local_data_source.SearchLocalDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalDataSourceModule {

    @Binds
    @Singleton
    abstract fun bindProductLocalDataSource(productLocalDataSourceImpl: ProductLocalDataSourceImpl): ProductLocalDataSource

    @Binds
    @Singleton
    abstract fun bindSearchLocalDataSource(searchLocalDataSourceImpl: SearchLocalDataSourceImpl): SearchLocalDataSource
}
