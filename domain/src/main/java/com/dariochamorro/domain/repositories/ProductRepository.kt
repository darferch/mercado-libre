package com.dariochamorro.domain.repositories

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.models.ProductDetail
import kotlinx.coroutines.flow.Flow

interface ProductRepository {

    suspend fun productsByQuery(query: String): AsyncResult<List<Product>>

    suspend fun productDetailById(id: String): AsyncResult<ProductDetail>

    fun recentProducts(): Flow<AsyncResult<List<Product>>>

    suspend fun deleteProductFromRecent(product: Product): AsyncResult<Unit>
}
