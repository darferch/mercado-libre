package com.dariochamorro.testing_commons.samples

import com.dariochamorro.domain.models.Product

class ProductSample : DataSample<Product>() {
    override fun single(index: Int): Product = Product(
        id = "$index",
        title = "Title $index",
        price = 1000.0 * index,
        availableQuantity = index + 1,
        condition = "new",
        image = "Image $index"
    )
}
