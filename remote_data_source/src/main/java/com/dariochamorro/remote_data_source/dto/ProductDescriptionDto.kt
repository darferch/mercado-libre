package com.dariochamorro.remote_data_source.dto

data class ProductDescriptionDto(
    val pain_text: String?
)
