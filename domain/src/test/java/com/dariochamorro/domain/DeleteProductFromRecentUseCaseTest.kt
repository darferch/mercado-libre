package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.usescases.DeleteProductFromRecentUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class DeleteProductFromRecentUseCaseTest : CoroutineUnitTest() {

    @Mock
    lateinit var repository: ProductRepository
    lateinit var deleteProductFromRecentUseCase: DeleteProductFromRecentUseCase

    private val product = SampleFactory.single(Product::class)

    @Before
    fun setup() {
        deleteProductFromRecentUseCase = DeleteProductFromRecentUseCase(repository, testDispatchers)
    }

    @Test
    fun `should return a Success if it is invoked`() =
        runBlockingTest {
            // Given
            val resultExpected = AsyncResult.success(Unit)
            whenever(repository.deleteProductFromRecent(product)).thenReturn(resultExpected)

            // When
            val result = deleteProductFromRecentUseCase(product)

            // Then
            assertEquals(resultExpected, result)
        }

    @Test
    fun `should return a Error if deleteProductFromRecent fail`() = runBlockingTest {
        // Given
        val resultExpected = AsyncResult.error<Unit>(Errors.DatabaseError)
        whenever(repository.deleteProductFromRecent(product)).thenReturn(resultExpected)

        // When
        val result = deleteProductFromRecentUseCase(product)

        // Then
        assertEquals(resultExpected, result)
    }
}
