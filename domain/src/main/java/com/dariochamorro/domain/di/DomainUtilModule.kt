package com.dariochamorro.domain.di

import com.dariochamorro.domain.util.DispatcherProvider
import com.dariochamorro.domain.util.DispatcherProviderImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DomainUtilModule {
    @Binds
    @Singleton
    abstract fun bindDispatcherProvider(dispatcherProviderImpl: DispatcherProviderImpl): DispatcherProvider
}
