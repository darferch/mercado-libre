# Android Test Mercado Libre


### Dario Fernando Chamorro Vela
dario.chamorro.v@gmail.com

57 3017790935

&nbsp;

## Description
Search products and see their detail

&nbsp;

## Requirements to run the application
- Android Studio 4.1.2

&nbsp;

## UI/UX
The application was designed with [Figma](https://www.figma.com/) and you can see the prototype in this [link](https://www.figma.com/file/DcwpMvvHOnnAIirltzjnrK/Mercado-Libre?node-id=5%3A14) 

![Preview](preview.gif)

&nbsp;

## Solution Architecture

In order to propose a solution that is tolerant to change and that is easy to scale over time, I was decided to use Clean Architecture, segmenting the application into three layers Presentation, Domain and Data.


### **Domain**
The domain layer implements the Business logic and rules

- **Models**:
 The Models can implement structures and business rules, in this case the information of the Post, Comments and Detail

- **Use Case**:
The use cases allow interaction with the architecture and implement the business logic

- **Repository**:
The repositories abstract the sources of information, in the domain layer only the behavior is defined but its implementation is unknown, this so that the data layer depends on the domain layer

### **Presentation**
The presentation layer includes the Activities or Fragments views, and state management through ViewModel and LiveData

### **Data**
The data layer implements the repositories defined in the domain layer and it define the behavior of the data sources

![Architecture](arch.png)


&nbsp;


## Code Quality

### **Testing**

There are different types of tests (Unit, Integration, Acceptance), for the implementation of this project it was chosen to use TDD (Test Driven Development) where the tests are first implemented in order to design the code.

- By time, TDD was only performed in use cases, repositories and view models.
- Instrumentation tests (Android Test) were not implemented due to time constraints.

### **Ktlint**
In order to have a code with the same style and format, [ktlint](https://github.com/pinterest/ktlint) was added that allows adding rules in the code, this is executed before to build task

### **Crashlytics**
Firebase Crashlytics was configured in the project,  in order to register error logs in the application and be able to see them in the firebase console

### **Continuous Integration**
Added gitlab-ci.yml to execute scripts for build and test in each Merge Request

&nbsp;

## Libraries
Official android libraries were used or that are widely used by the development community.

### **Retrofit**
Library to implement an Http Client through interfaces which is compatible with the Dependency Inversion pattern

### **Room**
Library for data access with SQLite, official for Android, allows creating DAOs with interfaces so it is compatible with the Dependency Inversion pattern

### **Android ViewModel** 
Set of official android libraries for the use of ViewModels to handle the view state

### **Android LiveData** 
Set of official android libraries emit updates of the state from ViewModel to View

### **Hilt**
Official android library for dependency injection with Dagger, it greatly facilitates the injection of dependencies so it is very useful

### **Mockito** 
Library to create test mocks and stubs

&nbsp;


## Good Practices

In the development of the project, it was sought to implement a set of good practices:

 - CLean Architecture
 - SOLID Patterns
 - KtLint
 - TDD
 - Continuous Integration
