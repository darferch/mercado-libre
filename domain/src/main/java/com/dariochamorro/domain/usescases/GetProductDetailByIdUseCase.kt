package com.dariochamorro.domain.usescases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.models.ProductDetail
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetProductDetailByIdUseCase @Inject constructor(
    private val productRepository: ProductRepository,
    private val dispatchers: DispatcherProvider
) {
    suspend operator fun invoke(id: String): AsyncResult<ProductDetail> =
        withContext(dispatchers.io()) {
            productRepository.productDetailById(id)
        }
}
