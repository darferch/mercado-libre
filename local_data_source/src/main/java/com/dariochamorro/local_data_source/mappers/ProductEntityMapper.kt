package com.dariochamorro.local_data_source.mappers

import com.dariochamorro.domain.common.ModelMapper
import com.dariochamorro.domain.models.Product
import com.dariochamorro.local_data_source.entities.ProductEntity
import javax.inject.Inject

class ProductEntityMapper @Inject constructor() : ModelMapper<Product, ProductEntity>() {
    override fun toModel(data: ProductEntity): Product = Product(
        id = data.productId,
        title = data.title,
        price = data.price,
        availableQuantity = data.availableQuantity,
        condition = data.condition,
        image = data.image
    )
}
