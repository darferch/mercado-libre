package com.dariochamorro.testing_commons.samples

class StringSample : DataSample<String>() {
    override fun single(index: Int): String = "Sample $index"
}
