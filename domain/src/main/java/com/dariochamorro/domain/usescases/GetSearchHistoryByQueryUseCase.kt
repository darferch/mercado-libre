package com.dariochamorro.domain.usescases

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.repositories.SearchHistoryRepository
import com.dariochamorro.domain.util.DispatcherProvider
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetSearchHistoryByQueryUseCase @Inject constructor(
    private val searchHistoryRepository: SearchHistoryRepository,
    private val dispatchers: DispatcherProvider
) {
    suspend operator fun invoke(query: String): AsyncResult<List<String>> =
        withContext(dispatchers.io()) {
            searchHistoryRepository.searchHistoryByQuery(query)
        }
}
