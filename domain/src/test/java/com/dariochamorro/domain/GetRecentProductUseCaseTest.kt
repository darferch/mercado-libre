package com.dariochamorro.domain

import com.dariochamorro.domain.common.AsyncResult
import com.dariochamorro.domain.common.Errors
import com.dariochamorro.domain.models.Product
import com.dariochamorro.domain.repositories.ProductRepository
import com.dariochamorro.domain.usescases.GetRecentProductsUseCase
import com.dariochamorro.testing_commons.CoroutineUnitTest
import com.dariochamorro.testing_commons.samples.SampleFactory
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class GetRecentProductUseCaseTest : CoroutineUnitTest() {
    @Mock
    lateinit var repository: ProductRepository
    lateinit var getRecentProductsUseCase: GetRecentProductsUseCase

    @Before
    fun setup() {
        getRecentProductsUseCase = GetRecentProductsUseCase(repository, testDispatchers)
    }

    @Test
    fun `should emit a flow of Products when it is invoked`() =
        runBlockingTest {
            // Given
            val data = SampleFactory.list(Product::class, 2)
            val resultExpected = AsyncResult.success(data)
            whenever(repository.recentProducts()).thenReturn(listOf(resultExpected).asFlow())

            // When
            var result: AsyncResult<List<Product>>? = null
            getRecentProductsUseCase().collect { result = it }

            // Then
            assertEquals(resultExpected, result)
        }

    @Test
    fun `should emit a error if recentProducts fail`() = runBlockingTest {
        // Given
        val resultExpected = AsyncResult.error<List<Product>>(Errors.DatabaseError)
        whenever(repository.recentProducts()).thenReturn(listOf(resultExpected).asFlow())

        // When
        var result: AsyncResult<List<Product>>? = null
        getRecentProductsUseCase().collect { result = it }

        // Then
        assertEquals(resultExpected, result)
    }
}
